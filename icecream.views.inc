<?php

/**
 * Implements hook_views_data_alter().
 */
function icecream_views_data_alter(&$data) {
  if (isset($data['users'])) {

    // Extend user views implementation with ice cream applied boolean field
    $data['users']['user_icecream_applied'] = array(
      'title' => t('Applied for ice cream'),
      'help' => t('Whether a user applied for ice cream or not.'),
      'real field' => 'uid',
      'field' => array(
        'id' => 'user_icecream_applied',
      ),
      'filter' => array(
        'id' => 'user_icecream_applied',
        'label' => t('Applied for ice cream'),
        'type' => 'yes-no',
      ),
      'sort' => array(
        'id' => 'standard',
      ),
    );
  }
}