<?php
/**
 * @file
 * Contains \Drupal\icecream\Controller\IcecreamController.
 */

namespace Drupal\icecream\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * IcecreamController.
 */
class IcecreamController extends ControllerBase {

  protected $icecreamService;

  /**
   * Class constructor.
   */
  public function __construct($icecreamService) {
    $this->icecreamService = $icecreamService;
  }


  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('icecream.statistics')
    );
  }


  /**
   * Displays a page announcing one can get ice cream now.
   */
  public function its_icecream_time() {
    return array(
      '#statistics' => $this->icecreamService,
      '#theme' => 'icecream_time',
      '#attached' => [
        'css' => [
          drupal_get_path('module', 'icecream') . '/css/icecream.css'
        ]
      ]
    );
  }


  /**
   * Checks access for the ice cream time page.
   *
   * NOTE: this is a simplified way of creating an access check.  If you want to write reusable access checks using a
   * service, please check: https://www.drupal.org/node/2122195
   */
  public function access() {
    // Show a message if "ice cream time" is reached, and links to the internal route name
    return AccessResult::allowedIf($this->icecreamService->isIcecreamTime());
  }

}