<?php

/**
 * @file
 * Contains Drupal\icecream\IcecreamManager.
 *
 * This file is not used, but serves as an example on how to make your code more testable by using Dependency Injection.
 */

namespace Drupal\icecream;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;

class IcecreamManager {

  /**
   * Database connection
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Ice cream settings config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs the ice cream manager service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current database connection.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $connection) {
    $this->configFactory = $config_factory;
    $this->connection = $connection;
  }

  /**
   * Indicates whether it is ice cream time.
   *
   * @return bool
   *   Returns TRUE if it is ice cream time, FALSE otherwise.
   */
  public function isIcecreamTime() {
    return $this->getThreshold() <= $this->getNumberApplied();
  }

  /**
   * The number of users who applied for ice cream.
   *
   * @return int
   *   The number of users who applied for ice cream.
   */
  public function getNumberApplied() {
    $query = "SELECT COUNT(*) FROM {users_data} WHERE module = 'icecream' AND name = 'applied' AND value = '1'";
    return (integer) $this->connection->query($query)->fetchField();
  }


  /**
   * @return int
   *   The number of users required before it is ice cream time.
   */
  public function getThreshold() {
    return $this->configFactory->get('icecream.settings')->get('threshold');
  }

}