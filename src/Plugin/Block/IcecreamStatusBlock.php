<?php

namespace Drupal\icecream\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides either "A button to show ice cream interest" or "A message telling its ice cream time".
 *
 * @Block(
 *   id = "icecream_status_block",
 *   admin_label = @Translation("Icecream status"),
 * )
 */
class IcecreamStatusBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /** @var \Drupal\icecream\IcecreamManager $icecream_statistics_service */
    $icecream_statistics_service = \Drupal::service('icecream.statistics');

    // Show a message if "ice cream time" is reached, and links to the internal route name
    if ($icecream_statistics_service->isIcecreamTime()) {
      return array(
        '#markup' => t('Yaaay!  Time to <a href="@url">party</a>!', array('@url' => \Drupal::url('icecream.its_icecream_time'))));
    }


    // Fetch the "applied" value from the user.data service, result will be either NULL or True
    $account = \Drupal::currentUser();
    $applied_for_icecream = \Drupal::service('user.data')->get('icecream', $account->id(), 'applied');

    // Show a message if the current user already applied for ice cream.
    if ($applied_for_icecream == TRUE) {
      return array('#markup' => "You applied for ice cream.");
    }


    // Show "Apply for ice cream" form.
    $elements = \Drupal::formBuilder()->getForm('Drupal\icecream\Form\IcecreamApplyForm');
    return $elements;

  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    return $account->isAuthenticated();
  }

}