<?php

/**
 * @file
 * Definition of Drupal\icecream\Plugin\views\field\UserAppliedForIcecream.
 */

namespace Drupal\icecream\Plugin\views\field;

use Drupal\views\Plugin\views\field\Boolean;
use Drupal\views\ResultRow;


/**
 * Field handler to display the whether a user applied for ice cream or not.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("user_icecream_applied")
 */
class UserAppliedForIcecream extends Boolean {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $uid = $this->getValue($values);
    $applied_for_icecream = \Drupal::service('user.data')->get('icecream', $uid, 'applied');

    if ($this->options['type'] == 'custom') {
      return $applied_for_icecream ? UtilityXss::filterAdmin($this->options['type_custom_true']) : UtilityXss::filterAdmin($this->options['type_custom_false']);
    }
    elseif (isset($this->formats[$this->options['type']])) {
      return $applied_for_icecream ? $this->formats[$this->options['type']][0] : $this->formats[$this->options['type']][1];
    }
    else {
      return $applied_for_icecream ? $this->formats['yes-no'][0] : $this->formats['yes-no'][1];
    }
  }


}
