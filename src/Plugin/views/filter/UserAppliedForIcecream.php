<?php

/**
 * @file
 * Definition of views_handler_filter_user_icecream_applied.
 */

namespace Drupal\icecream\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\filter\BooleanOperator;

/**
 * Filter handler for the current user.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("user_icecream_applied")
 */
class UserAppliedForIcecream extends BooleanOperator {

  /**
   * Overrides Drupal\views\Plugin\views\filter\BooleanOperator::init().
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->value_value = t('Applied for ice cream');
  }

  public function query() {

    // Fetch current query
    $users = $this->ensureMyTable();

    // Create our new condition
    $icecream_condition = db_and();


    if ($this->value != '1') {
      // If user did not apply, there is no record for him, lets fetch all users without user_data records for icecream
      $query = "SELECT uid FROM {users_data} WHERE module = 'icecream' AND value = 1";
      $result = db_query($query);
      $uids = array(0); // Provide default value to ensure NOT IN will not crash
      foreach ($result as $record) {
        $uids[] = $record->uid;
      }
      $icecream_condition->condition("$users.uid", $uids, 'NOT IN');
    }
    else {

      // Create join to user_data
      $definition = array(
        'table' => 'users_data',
        'field' => 'uid',
        'left_table' => 'users',
        'left_field' => 'uid',
      );
      $join = \Drupal::service('plugin.manager.views.join')->createInstance('standard', $definition);
      $user_data = $this->query->addRelationship('users_data', $join, $users);

      $icecream_condition = db_and();
      $icecream_condition->condition("$user_data.module", "icecream");
      $icecream_condition->condition("$user_data.name", "applied");
      $icecream_condition->condition("$user_data.value", 1);
    }


    // Add conditions
    $this->query->addWhere($this->options['group'], $icecream_condition);
  }

}
