<?php

/**
 * @file
 * Contains \Drupal\icecream\Form\IcecreamApplyForm.
 */

namespace Drupal\icecream\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to apply for ice cream.
 */
class IcecreamApplyForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['apply'] = array(
      '#type' => 'submit',
      '#value' => $this->t('I want ice cream!'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if ($this->currentUser()->id()) {
      // Use the Drupal user.data service to store current user applied for ice cream.
      \Drupal::service('user.data')->set('icecream', $this->currentUser()->id(), 'applied', True);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icecream_apply_form';
  }
}
