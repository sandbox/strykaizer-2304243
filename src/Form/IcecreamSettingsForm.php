<?php

/**
 * @file
 * Contains \Drupal\icecream\Form\IcecreamSettingsForm.
 */

namespace Drupal\icecream\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Ice cream settings for this site.
 */
class IcecreamSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'icecream_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Load config
    $config = $this->config('icecream.settings');

    // Create form
    $form['icecream_reset'] = array(
      '#type' => 'details',
      '#title' => t('Reset'),
      '#open' => TRUE,
    );

    $form['icecream_reset']['reset_all'] = array(
      '#type' => 'submit',
      '#value' => t('Reset ice cream count'),
      '#submit' => array(array($this, 'submitResetIcecream')),
    );

    $form['icecream_config'] = array(
      '#type' => 'details',
      '#title' => t('Config'),
      '#open' => TRUE,
    );

    $form['icecream_config']['threshold'] = array(
      '#type' => 'number',
      '#title' => $this->t('User threshold'),
      '#attributes' => array(
        'min' => 0,
        'value' => $config->get('threshold'),
      ),
      '#description' => $this->t('The required number of users who applied for ice cream, before "ice cream time" will trigger'),
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('icecream.settings')
      ->set('threshold', $form_state->getValue('threshold'))
      ->save();

    parent::submitForm($form, $form_state);
  }


  /**
   * Reset ice cream user data.
   */
  public function submitResetIcecream(array &$form, FormStateInterface $form_state) {
    // Delete all data in users_data related to icecream
    db_delete('users_data')
      ->condition('module', 'icecream')
      ->execute();
    drupal_set_message(t('Ice cream reset performed.'));
  }

}

